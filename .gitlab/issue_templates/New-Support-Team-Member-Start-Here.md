```
module: New Support Team Member Start Here
area: Making Support Better
level: Beginner
maintainer: vijirao
pathways:
  support-engineer-onboarding:
    position: 1
  support-manager-onboarding:
    position: 1
```

## Introduction

Welcome to the Support Team, we are so excited that you've joined us!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#general-guidelines-1) is OK!**

**Your Onboarding Buddy:** `[tag buddy]`

**Goals of this checklist**

Keep this issue open until you complete the onboarding pathway modules shown below for your role to track your onboarding modules. Once you have completed them and had a discussion with your manager, close this issue.

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/); the page also shows you the different modules you'll need to complete as part of your onboarding, duration, and other general information.
- It should take you approximately **5 to 6 weeks to complete** all the modules that make up your onboarding pathway.

1. [ ] Edit this issue's description to remove the irrelevant path based on your role below.

## Support Engineer onboarding pathway

Note: When you've completed the onboarding pathway, speak with your manager and choose your first area of focus (usually "GitLab.com SAAS Support" or "Self-Managed Support")

1. New Support Team Member Start Here
1. [ ] Git & GitLab Basics
1. [ ] Installation & Administration Basics
1. [ ] GitLab Support Basics
1. [ ] Customer Service Skills
1. [ ] Working on Tickets
1. [ ] Zendesk Basics

## Support Manager onboarding pathway

Note: Once you complete the below, you may also wish to complete "Installation & Administration Basics" and "Customer Service Skills" from the Support Engineer onboarding pathway.

1. New Support Team Member Start Here
1. [ ] Support Manager Basics
1. [ ] Git & GitLab Basics
1. [ ] GitLab Support Basics
1. [ ] Zendesk Basics

## Onboarding Feedback & Documentation

Keeping our documentation and workflows up to date will ensure that all Support team members will be able to access and learn from the best practices of the past. Giving feedback about your onboarding experience will ensure that this document is always up to date, and those coming after you will have an easier time coming in.

1. [ ] Schedule a call with your manager to discuss your onboarding issue or integrate this into your 1:1. Answer the following questions:
    - What was most helpful?
    - What do you wish existed in your onboarding, but does not?
    - In which areas do you still not feel confident?
    - In which areas do you feel strong?
  1. [ ] Make an update to one or more Support training module templates to make it better and link them below. The files are located in an issue template in the ['support-training` repository](/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).
    1. _________

#### Congratulations on completing your Support Onboarding modules successfully!

/label ~onboarding
