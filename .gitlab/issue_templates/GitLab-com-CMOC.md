---
module: GitLab.com CMOC
area: Helping Customers
level: Beginner
maintainer: TBD
pathways:
  gitlab-com-saas-support:
    position: 2
---

<!--

**Title:** *"GitLab.com CMOC - **your-name**"*

-->

**Goal of this module:** Instruct the taker on the duties and responsibilities of being the [Communications Manager On Call (CMOC)](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities) for an active GitLab.com incident. This includes providing a clear understanding of what an incident is, how to work with reliability engineering during one, and how to use the tools at our disposal to effectively communicate updates to incidents both internally and externally to end-users and stakeholders.

Tackle each stage in sequential order, **but first**:

+ [ ] Ping your manager on this issue to notify them you have started.
+ [ ] Open an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single+Person+Access+Request) to request access to Status.io and PagerDuty (if you don't already have an account).
    Note: Make sure you request access to the `Test Page` along with the production page on status.io - this is the testing environment and you'll need this to complete Stage 4 of this issue.

## Stage 1: GitLab.com Architecture, Monitoring, and Incident Basics

+ [ ] Done with Stage 1

A basic understanding of the architecture that the GitLab.com platform is comprised of along with how our infrastructure team monitors it is paramount to understanding how issues with certain components of the platform affect end-users. It's also essential to know what an incident is at its most basic level and how we classify one.

### Architecture & Monitoring

+ [ ] Read through the [Production Architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/) document to gain a basic understanding of the infrastructural layout of GitLab.com.
+ [ ] Read about the [Monitoring of GitLab.com](https://about.gitlab.com/handbook/engineering/monitoring/) to understand how our infrastructure team monitors the performance of GitLab.com.
+ [ ] Read about which [critical dashboards](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#critical-dashboards) show if GitLab.com is experiencing an incident and then bookmark the following ones.
  + [Triage](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s)
  + [General Triage](https://dashboards.gitlab.net/d/general-triage/platform-triage?orgId=1)

### Incident Basics

+ [ ] Read the [Incident Management](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/) page from the Infrastructure section of the GitLab handbook. Take special note of:
  + [What an incident is](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#incident-management).
  + What [roles](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities) are assumed during an incident.
  + The [definitions](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#status) of the different state of operations that the GitLab.com platform may be in during an incident.
  + Be aware of [PagerDuty schedules](https://about.gitlab.com/handbook/support/support-ops/responsibilities.html#pagerduty-schedules), specifically the hours of on-call. They may be slightly outside of your normal hours of work.
  + Understand the [expectations](https://about.gitlab.com/handbook/support/on-call/#expectations-for-on-call) for being on-call

## Stage 2: Incident Preparation

+ [ ] Done with Stage 2

Your workspace should be configured to be as prepared as possible for an incident. This means having essential issue trackers bookmarked, joining incident management related Slack channels, and being aware of where reports of issues from end-users will surface.

### Issue Trackers

+ [ ] Bookmark these issues trackers.
  + [Production](https://gitlab.com/gitlab-com/gl-infra/production/issues)
  + [CMOC Handover](https://gitlab.com/gitlab-com/support/dotcom/cmoc-handover/issues)

### Slack Channels

+ [ ] Join these Slack channels
  + [#production](https://gitlab.slack.com/messages/C101F3796)
  + [#incident-management](https://gitlab.slack.com/messages/CB7P5CJS1)

Optionally, consider joining the following channels as well. They aren't necessary to monitor when working through most incidents but they will be useful eventually.

+ [#mgcp_gitlab_ops](https://gitlab.slack.com/archives/CQUA8FYJ3)
+ [#ongres-gitlab](https://gitlab.slack.com/archives/CLJ2QK0TT)
+ [#cloud-provider-alerts](https://gitlab.slack.com/archives/CBL6LVDHT)
+ [#alerts](https://gitlab.slack.com/archives/C12RCNXK5)
+ [#alerts-general](https://gitlab.slack.com/archives/CD6HFD1L0)
+ [#announcements](https://gitlab.slack.com/archives/C8PKBH3M5)
+ [#dev-escalation](https://gitlab.slack.com/archives/CLKLMSUR4)

### GitLab Community Forum

Reports of problems with GitLab.com will primarily come from end-users through Zendesk but they may come from other sources as well.

+ [ ] Bookmark the [GitLab Community Forum](https://forum.gitlab.com/) and be prepared to check it for reports of issues with GitLab.com.

## Stage 3: Managing Incidents

+ [ ] Done with Stage 3

The incident management process normally begins with a PagerDuty page to the CMOC from the EOC when they've detected a problem with GitLab.com severe enough to require us.

However, if we or our users notice an issue that we suspect could turn into an incident we're encouraged to contact the oncall EOC for their opinion.

+ [ ] Read about [how to determine who the oncall EOC is](https://about.gitlab.com/handbook/on-call/#reporting-issues-on-gitlabcom).

In more severe cases if we've received enough reports from users of a particular issue with GitLab.com that we feel is indicative of an incident we can page the oncall EOC through PagerDuty.

+ [ ] Read about [how to page to oncall EOC](https://about.gitlab.com/handbook/on-call/#pagerduty).

As a general rule, at least **three** separate reports of the same type of issue within a relatively short timeframe is a strong indicator that GitLab.com may be facing an incident and a page to the EOC is justified. Be sure to include links to Zendesk tickets and any other information to the EOC that they may need to understand the issue clearly and concisely.

### The Status Page - Status.io

Once an incident has [been declared](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#declaring-an-incident) the EOC will page the CMOC via PagerDuty and it'll be up to you to start managing Status.io. Along with this, one of your first tasks should be to join **The Situation Room** Zoom call so that you can follow along with the EOC and anyone else involved in working the incident.

+ [ ] Read about [how to conduct yourself while in The Situation Room](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#the-situation-room).

Effective communication with production engineering during an incident is crucial as the content of our status updates will largely come from them. Keeping yourself informed on the progress of an incident will allow you to communicate updates quickly and concisely with stakeholders and affected users.

Incidents and maintenance events are managed by the CMOC throughout their entire lifecycle through our [status page](https://status.gitlab.com/), powered by [Status.io](https://status.io). Updates made to incidents and maintenance events through Status.io are automatically tweeted out via [@gitlabstatus](https://twitter.com/gitlabstatus) through the [broadcast feature](https://kb.status.io/notifications/twitter-notifications/) of Status.io.

+ [ ] Learn how to [Create, Update, and Resolve](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#managing-incidents) incidents in Status.io.
+ [ ] Read about [Frequency of Updates](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#frequency-of-updates) to learn how often you should aim to update Status.io depending on the severity of the incident.
+ [ ] Learn how to [Perform a Handover](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#handover-procedure) at the end of your on-call shift.

## Stage 4: Review

+ [ ] Done with Stage 4

### Review Past Incidents

+ [ ] Review the following past incidents to get an idea of the proper tone to use and what details to include when posting status updates.

#### [S2] Errors returned on CI job artifact uploads

A recent deployment to GitLab.com began to cause an issue with CI job artifacts returning a `500` error on upload.

+ [Status.io link](https://app.status.io/dashboard/5b36dc6502d06804c08349f7/incident/5ea9aa22fd5c8f04c38e6a95/edit)
+ [Status Page link](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/5ea9aa22fd5c8f04c38e6a95)
+ [Production Issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/2031)
+ [RCA](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9993)

#### [S3] Increased Error Rate on GitLab.com

Infrastructure was alerted to a CPU saturation issue on a specific Gitaly node which was causing overall slowness and timeouts on GitLab.com. This incident involved the CMOC blocking a .com user and contacting them via Zendesk.

+ [Status.io link](https://app.status.io/dashboard/5b36dc6502d06804c08349f7/incident/5e6fe1ac51323704c4d5818d/edit)
+ [Status Page link](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/5e6fe1ac51323704c4d5818d)
+ [Production Issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/1774)
+ [RCA](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9550)

#### [S1] GitLab.com Registry Issues

The GitLab.com registry began returning `503` for pushes and pulls.

+ [Status.io link](https://app.status.io/dashboard/5b36dc6502d06804c08349f7/incident/5e39de68593d1b04c4620391/edit)
+ [Status Page link](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/5e39de68593d1b04c4620391)
+ [Production Issue](https://gitlab.com/gitlab-com/gl-infra/production/issues/1624)
+ [RCA](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/661)

#### [S1] CI Runner Delays

A deployment to GitLab.com temporarily rendered sidekiq inoperable causing CI pipelines to not be picked up.

+ [Status.io link](https://app.status.io/dashboard/5b36dc6502d06804c08349f7/incident/5df8099f7d974315694d7d45/edit)
+ [Status Page link](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/5df8099f7d974315694d7d45)
+ [Production Issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/1498)
+ [RCA](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/8729)

If you need to review more examples, browse the [incident history](https://app.status.io/dashboard/5b36dc6502d06804c08349f7/incidents) section of Status.io.

### Status Page Testing

Our [Status.io](https://status.io) instance gives us access to a testing environment that allows you to create, update, and resolve incidents on our status page in an internal environment.

Once your access request has been fulfilled and you've logged in to Status.io you can access our testing environment by clicking the `GitLab System Status` dropdown box in the top right of the window and selecting `Test Page`. **If you continue without doing this you will be creating an incident on our LIVE status page**.

Test your knowledge by creating an incident, guide it through the entire incident lifecycle, and then provide a link to it.

+ [ ] Create, update, change to monitoring, and then resolve one incident on our test status page.
  + Link: 

## Completion

+ [ ] Ping your manager on this issue to let them know that you've completed this bootcamp and are ready to be added to the CMOC schedule rotation in PagerDuty.
+ [ ] Send an MR to declare yourself a **GitLab.com CMOC** on the team page.

/assign me
/due in 8 weeks
