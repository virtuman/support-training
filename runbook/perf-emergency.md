# Introduction

The goal of this runbook is to offer to the oncall support engineer tools to quickly gather information about the situation and context of performance problem. An emergency by itself is already a stressful situation. In an performance emergency, the situation can worsen due to the wide range of possibilities. This guide helps by documenting and restricting the first steps methodically, so that we can also narrow down the search range further on.

This guide is not a mechanical method to solve all performance issues. Solving those issues requires insight and experience. This guides intends to help navigate the issue, allowing for engineer insight and experience to shine.

- Problem statement
- GitLab SOS
- Going through system metrics
- Workload characterization
- Storage benchmarking with fio
- Tracing
- Profiling

# Problem statement

Quoting [Brendan Gregg in Thinking Methodically about Performance](https://queue.acm.org/detail.cfm?id=2413037):

>The intent is to collect a detailed description of the issue—the problem statement—which directs deeper analysis. The description on its own may even solve the issue. This is typically entered into a ticketing system by asking the following questions:

- What makes you think there is a performance problem?
- Has this system ever performed well?
- What has changed recently? (Software? Hardware? Load?)
- Can the performance degradation be expressed in terms of latency or runtime?
- Does the problem affect other people or applications (or is it just you)?
- What is the environment? What software and hardware are used? Versions? Configuration?

We can ask whether the issue is happening for a specific user, repo or group. Sometimes this is enough to clarify what is the root cause of the issue. This might also just help narrow down the possibilities.

# GitLab SOS and configuration

Be sure to ask for a [GitLab SOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos) or [KubeSOS](https://gitlab.com/gitlab-com/support/toolbox/kubesos) and a sanitized gitlab.rb at this point. It will capture lots of information that might be useful. Recording it in the ticket might also help other engineers analyze it async and offer you insight.

## Review major system metrics

Below are the major system metrics captured you may want to review while on the call.  Most of these are also captured by [GitLab SOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos).

- [uptime](http://man7.org/linux/man-pages/man1/uptime.1.html)
- [dmesg | tail](http://man7.org/linux/man-pages/man1/dmesg.1.html)
- [vmstat 1](http://man7.org/linux/man-pages/man8/vmstat.8.html)
- [mpstat -P ALL 1](http://man7.org/linux/man-pages/man1/mpstat.1.html)
- [pidstat 1](http://man7.org/linux/man-pages/man1/pidstat.1.html)
- [iostat -xz 1](http://man7.org/linux/man-pages/man1/iostat.1.html)
- [free -m](http://man7.org/linux/man-pages/man1/free.1.html)
- [sar -n DEV 1](http://man7.org/linux/man-pages/man1/sar.1.html)
- [sar -n TCP,ETCP 1](http://man7.org/linux/man-pages/man1/sar.1.html)
- [top](http://man7.org/linux/man-pages/man1/top.1.html)
- [iotop](http://man7.org/linux/man-pages/man8/iotop.8.html)

A quick rundown on how to interpret these tools can be found in [this blog post by Brendan Gregg](https://netflixtechblog.com/linux-performance-analysis-in-60-000-milliseconds-accc10403c55).

# Workload characterization

At this juncture, you have enough information to start thinking about the following questions, (again from [Thinking Methodically about Performance](https://queue.acm.org/detail.cfm?id=2413037)):

- Who is causing the load?
  - Process ID
  - GitLab user
  - Remote IP address
- What is triggering the load?
  - Rails controller
  - API route
  - Gitaly RPC
  - Sidekiq job
  - Kernel issue
  - Database locks, stuck queries
- What are other characteristics of the load?
  - [IOPS](https://en.wikipedia.org/wiki/IOPS)
    - [fio](https://linux.die.net/man/1/fio)
  - [Throughput](https://stackoverflow.com/questions/15759571/iops-versus-throughput)
    - [fio](https://linux.die.net/man/1/fio)
  - Type of load?
    - CPU
      - [vmstat](http://man7.org/linux/man-pages/man8/vmstat.8.html)
      - [top](http://man7.org/linux/man-pages/man1/top.1.html)
      - [uptime](http://man7.org/linux/man-pages/man1/uptime.1.html)
    - IO wait
      - [vmstat `wa` column](http://man7.org/linux/man-pages/man8/vmstat.8.html)
- How is the load changing over time?
  - Consistent
  - Occuring at regular intervals
  - Seemingly at random

Quoting the article:

>This helps to separate problems of load from problems of architecture, by identifying the former.

To gather load information from the GitLab components itself, we can employ [fast-stats](https://gitlab.com/gitlab-com/support/toolbox/fast-stats). It helps by assembling performance statistics on the structured logs. This can help us quickly glance at what rails controllers or gitaly RPC calls might be ill-performing.

[fast-stats](https://gitlab.com/gitlab-com/support/toolbox/fast-stats) has [a mode where we can compare the performance of a structured log format against the performance of a standard](https://gitlab.com/gitlab-com/support/toolbox/fast-stats#-compare-c-compare_file). This might be interesting to see if any particular controller or RPCs are ill-performiing.

If the performance issue is specific to a page, there is also tracing information in the [performance bar](https://docs.gitlab.com/ee/administration/monitoring/performance/performance_bar.html). There you can see the SQL queries, Redis queries, gitaly calls, Elasticsearch calls and frontend resources involved. Performance bar will not be available if the page cannot load.

If this is the case (specific to a page) - downloading the performance bar JSON file and uploading it to the ticket can also prove very useful for async analysis.

We can get a trace of the database calls by [enabling active record logging](https://docs.gitlab.com/ee/administration/troubleshooting/debug.html#enabling-active-record-logging) on a [Rails console](https://docs.gitlab.com/ee/administration/troubleshooting/navigating_gitlab_via_rails_console.html) and then issuing the commands to which you'd like to see the database trace.

We could also run EXPLAIN/ANALYZE on queries directly from the Rails console, [as per our docs](https://docs.gitlab.com/ee/development/understanding_explain_plans.html#rails-console) using:

```ruby
require 'activerecord-explain-analyze'

# Some.query.explain(analyze: true), for example
Project.where('build_timeout > ?', 3600).explain(analyze: true)
```

# Filesystem Benchmarking

We also have the [filesystem benchmark](https://docs.gitlab.com/ee/administration/operations/filesystem_benchmarking.html) to study the underlying filesystem performance. Given the particular nature of GitLab's workload on the filesystem, sometimes we have to test whether the filesystem is suitable or might be the cause of performance issues of the application. It is difficult to offer categorical numbers, but if you see anything significantly slower than disk performance (2000 read IOPS, 700 write IOPS) be suspicious that a slow disk might be playing a role. If you see numbers close to SSD performance (30k read IOPS, 10k write IOPS), then it becomes less likely that the disk performance is related.

# Stracing

At this juncture, the idea is to select a process from the [GitLab architecture](https://docs.gitlab.com/ee/development/architecture.html#simplified-component-overview) and start investigating it deeper. Since unicorn/puma and gitaly run the bulk of our code, we usually pick them to investigate further, with a honorable mention to workhorse when we notice that the scenario that we're investigating might be failing there.

We can `strace` the process under investigation, which will capture the system calls that the processes invoke in the kernel and log them into the trace file. You can find a basic introduction on strace [here](http://www.brendangregg.com/blog/2014-05-11/strace-wow-much-syscall.html).

Here's the command to capture a trace file on all unicorn processes, it is necessary to leave it running for about 20 seconds to gather enough information. Please be aware that strace might also cause performance issues due to the additional overhead. In a performance emergency, however, we usually think that the situation is already untenable and thus we're willing to suffer the additional overhead if it gives us insight into how to get out of the situation:

```
ps auwx | grep unicorn | awk '{ print " -p " $2}' | xargs strace -tt -T -f -y -yy -s 1024 -o /tmp/unicorn.txt
```

The same command works analogously for puma:

```
ps auwx | grep puma | awk '{ print " -p " $2}' | xargs strace -tt -T -f -y -yy -s 1024 -o /tmp/puma.txt
```

Once you are done you can ctrl+c to stop the capture. It is a good practice to double-check that the trace indeed stopped by running:

```
ps auwx | grep strace
```

This should yield only the grep process itself. If something else appears, feel free to kill it.

To analyze the trace, we can begin by employing the [strace-parser](https://gitlab.com/gitlab-com/support/toolbox/strace-parser). It should provide us a overview of the child processes and system calls that the process is performing. We should use these as a "table of contents" for the actual trace file: pick an information from it and check its context in the actual trace file. You can find further info on how to read the strace output on the [strace quiz](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/content/strace_quiz.md).

Reading a trace file is a daunting task, but in essence it is similar to reading a log file: we should attempt to understand what the system calls are attempting to do one by one. Soon enough you'll be able to see patterns and thus be able to jump over the familiar ones and investigate the unfamiliar ones.

One trick that might get you started reading the trace file is to locate the call that is writing to the log. If we have a log entry that we'd like to dig deeper, we can start by finding the trace of the call writing to the log and then working backwards. This entry can be an error message, but it can also be the entry writing the duration of the request.

Another trick that might make reading the trace file more approachable is filtering out the calls to `epoll_wait`, `futex` and `clock_gettime`. This would allow us to focus on other calls that might signal better what the process is trying to do.

When the problem seems to be about networking, you might want to capture a `tcpdump` and analyze it with wireshark or `tshark`. You can learn more about how to perform a capture and how to analyze it in the [wireshark docs](https://www.wireshark.org/docs/), and specifically at the [wireshark's users guide](https://www.wireshark.org/docs/wsug_html_chunked/).

## Profiling

https://docs.gitlab.com/ee/administration/troubleshooting/gitlab_rails_cheat_sheet.html#profile-a-page
https://docs.gitlab.com/ee/administration/monitoring/performance/request_profiling.html

## Database

There are various instances where the database can be the one causing performance issues. Usual symptom is high CPU usage by the PostgreSQL processes, or high DB timings in the logs or performance bar.

If we think of the database as a potential issue, it's likely worth dumping all PG settings and uploading to the ticket for further analysis through:

```sql
select * from pg_settings;
```

### Unvacuumed/analyzed database after a version upgrade

More common than we expect, this can happen with either the bundled database when the post-upgrade analyze script doesn't run (example bug that could cause this in [omnibus-gitlab#5345](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/5345)) or for external databases ([AWS example in our docs](https://docs.gitlab.com/omnibus/settings/database.html#using-a-non-packaged-postgresql-database-management-server) asking to run a manual `ANALYZE VERBOSE;`) when the external upgrade process doesn't automatically analyze the tables and expects the user to do it manually but they don't.

The state of this could be checked by the following query in a `gitlab-psql` console:

```sql
SELECT relname, last_analyze, last_autoanalyze FROM pg_stat_user_tables;
```

Empty (NULL) values for the `last_analyze` and `last_autoanalyze` columns hint that they weren't analyzed and doing so manually could fix the issue. Doing this (as an emergency command) is possible through the psql console (note, we need to unset the timeout since it's 60s by default and on large tables, this won't finish in time).

```sql
SET statement_timeout=0;
ANALYZE VERBOSE;
```

### Locks / stuck queries

The PostgreSQL docs have a great section called [Lock Monitoring](https://wiki.postgresql.org/wiki/Lock_Monitoring).

There is an important note that some external databases don't have a default `idle_in_transaction_session_timeout` (meaning it's unlimited) or it's set very high (12 hours on AWS RDS PostgreSQL 11.1+) - this could actually block Sidekiq threads and prevent background jobs from processing if queries are stuck in this state. We [default to 1min](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/7517145fa13f5a0ddd7dda5e7f29575e7382056e/files/gitlab-cookbooks/postgresql/attributes/default.rb#L75) in Omnibus, so this shouldn't happen on Omnibus-bundled DBs.

To check for queries stuck in 'idle in transaction' we could use:

```sql
SELECT *
FROM pg_stat_activity
WHERE state = 'idle in transaction' AND (now() - query_start) > '10 minutes'::interval;
```

### Monitoring

TBD